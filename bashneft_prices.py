import csv

def get_gas_prices():
	with open('prices.csv', newline='') as csvfile:
		csvreader = csv.reader(csvfile, delimiter=';')
		data = []
		for row in csvreader:
			data.append(row)
		last_row = data[-1]

		return [last_row[0]] + list(map(float, list(map(lambda x: x.replace(',', '.'), last_row[1:]))))

if __name__ == '__main__':
	print(f'{get_gas_prices()[6]:.2f}')
	#print(get_gas_prices())