import hashlib
import requests
import os.path


url = 'https://bashneft-azs.ru/upload/medialibrary/preyskurant-{}/{}.JPG'.format('25-01-2020', 'Республика Башкортостан')
hashfile = 'hash.md5'

def read_hash(hashfile):
	if not os.path.isfile(hashfile):
		with open(hashfile, 'w') as f:
			pass
	with open(hashfile, 'r') as f:
		return f.read()

def calc_hash(content):
	md5 = hashlib.md5()
	md5.update(content)
	return md5.hexdigest()

if __name__ == '__main__':

	read_hash = read_hash(hashfile)
	p = requests.get(url)
	calc_hash = calc_hash(p.content)

	if read_hash != calc_hash:
		with open(hashfile, 'w', encoding='utf-8') as f:
			print(calc_hash, file=f)