from datetime import date
import calendar
import locale
import mysql.connector as mariadb

locale.setlocale(locale.LC_ALL, '')

def get_today():
	today = date.today()
	weekday = calendar.day_name[today.weekday()]
	months = ('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
	           'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря')
	bdays = ('рабочий день','выходной день','праздничный день', 'не знаю работаем или отдыхаем')

	conn = mariadb.connect(user='db_guest', password='123456', database='business_days')
	cursor = conn.cursor()
	cursor.execute('SELECT day FROM ba_calendar WHERE date = current_date')
	result = cursor.fetchone()
	if result != None:
		bday = result[0]
	else:
		bday = 3
	conn.close()

	return f'Сегодня {today.day} {months[today.month]}, {weekday}, {bdays[bday]}'

if __name__ == '__main__':
	print(get_today())