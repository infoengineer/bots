import telebot
import yaml
from messages import messages, get_text

telebot.apihelper.proxy = {'https': 'https://85.132.71.82:3128'}

with open('config.yaml') as f:
    config = yaml.safe_load(f)

bot = telebot.TeleBot(config['tg_token'])

@bot.message_handler(content_types=['text'])
def get_text_messages(message):
	bot.send_message(message.from_user.id, messages.get(get_text(message.text), 'Я тебя не понимаю. Напиши /help.'))

bot.polling(none_stop=True, interval=0)