from currency import get_value
from bashneft_prices import get_gas_prices
from business_days import get_today
from calend import get_calendar
import re

messages = {
'/start': 'Добро пожаловать вновь в соцсеть, товарищ!',
'Привет': 'Здарова, дружище, чем я могу тебе помочь?', 
'/help': 'Напиши Привет', 
'Пока': 'До встречи!',
'/usd': f'Курс доллара на сегодня {get_value("USD"):.2f}',
'/eur': f'Курс евро на сегодня {get_value("EUR"):.2f}',
'/today': get_today(),
'/92': f'92-й сегодня стоит {get_gas_prices()[5]:.2f}',
'/95': f'95-й сегодня стоит {get_gas_prices()[1]:.2f}',
'/diesel': f'Солярка сегодня стоит {get_gas_prices()[5]:.2f}',
'/calendar': get_calendar(),
}

def get_text(text):
	if re.search(r'usd|dollar|доллар', text, re.IGNORECASE):
		return '/usd'
	elif re.search(r'eur|евр', text, re.IGNORECASE):
		return '/eur'
	elif re.search(r'day|день|число', text, re.IGNORECASE):
		return '/today'
	elif re.search(r'92', text, re.IGNORECASE):
		return '/92'
	elif re.search(r'95', text, re.IGNORECASE):
		return '/95'
	elif re.search(r'diesel|дизел|соляр', text, re.IGNORECASE):
		return '/diesel'
	elif re.search(r'calendar|календарь', text, re.IGNORECASE):
		return '/calendar'
	else:
		return 'unclear'

if __name__ == '__main__':
	text = input()
	# print(get_text(text))
	print(messages[get_text(text)])
