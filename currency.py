from pycbrf import ExchangeRates
from datetime import datetime


def get_value(currency):
    rates = ExchangeRates(datetime.now(), locale_en=True)
    return float(rates[currency.upper()].value)


if __name__ == "__main__":
    print(get_value('USD'))