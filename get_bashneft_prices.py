try:
    from PIL import Image
except ImportError:
    import Image

import datetime

import requests
from urllib.parse import quote
from check import read_hash, calc_hash
import os
import csv

import pytesseract
pytesseract.pytesseract.tesseract_cmd = r'C:\Tesseract-OCR\tesseract.exe'

today = datetime.datetime.today()
fdate = '25-01-2020'
# fdate = today.strftime('%d-%m-%Y')

url = 'https://bashneft-azs.ru/upload/medialibrary/preyskurant-{}/Республика Башкортостан.JPG'.format(fdate)
original = 'bashneft_prices.jpg'

p = requests.get(url)
content = p.content

hashfile = 'hash.md5'

read_hash = read_hash(hashfile)
calc_hash = calc_hash(content)

if read_hash != calc_hash:
	with open(hashfile, 'w', encoding='utf-8') as f:
		print(calc_hash, file=f)

	out = open(original, 'wb')
	out.write(content)
	out.close()
	cropped = 'cropped_' + original
	img1 = Image.open(original)
	width = img1.size[0] # 561
	height = img1.size[1] # 666
	img2 = img1.crop((430, 227, width-40, height-70))
	img2.save(cropped)

	text = pytesseract.image_to_string(img2)
	fieldnames = ['date', 'ai100', 'ai95', 'ai95euro6', 'atum95', 'atum95euro6', 'ai92', 'atum92', 'diesel']
	row_data = [fdate]
	text_line = [line.rstrip() for line in text.splitlines() if line]
	row_data += list(map(float, list(map(lambda x: x.replace(',', '.'), text_line))))

	csv_filename = 'prices.csv'
	if not os.path.isfile(csv_filename):
		with open(csv_filename, 'w', newline='') as f:
			data = [fieldnames, row_data]
			writer = csv.writer(f, delimiter=';')
			for row in data:
				writer.writerow(row)
	else:
		with open(csv_filename, 'a+', newline='') as f:
			writer = csv.writer(f, delimiter=';')
			data = [row_data]
			for row in data:
				writer.writerow(row)

	os.remove(cropped)
	os.remove(original)

