import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
import random
import yaml
from messages import messages, get_text

def send_message(user_id, message):
    vk.method('messages.send', {'user_id': user_id, 'message': message, "random_id": random.getrandbits(64)})

with open('config.yaml') as f:
    config = yaml.safe_load(f)

token = config['vk_token']

vk = vk_api.VkApi(token=token)
longpoll = VkLongPoll(vk)

for event in longpoll.listen():
	try:
		if event.type == VkEventType.MESSAGE_NEW:
			if event.to_me:
				send_message(event.user_id, messages.get(get_text(event.text), 'Я тебя не понимаю. Напиши /help.'))
	except requests.exceptions.ConnectionResetError:
		longpoll = VkLongPoll(vk)
    