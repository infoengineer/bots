import calendar
import locale

locale.setlocale(locale.LC_ALL, '')

def get_calendar():
	cal = calendar.TextCalendar(firstweekday=0)
	return cal.formatyear(2020, 1, 2, c=3, m=2)

if __name__ == '__main__':
	print(get_calendar())